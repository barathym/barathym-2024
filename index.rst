

.. raw:: html

   <a rel="me" href="https://kolektiva.social/@raar"></a>
   <a rel="me" href="https://piaille.fr/@raar"></a>
   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://framapiaf.org/@goldman_bakounine"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>


.. https://framapiaf.org/web/tags/raar.rss
.. https://framapiaf.org/web/tags/pourim.rss
.. https://framapiaf.org/web/tags/golem.rss
.. https://framapiaf.org/web/tags/racisme.rss
.. https://framapiaf.org/web/tags/feminisme.rss

.. 🚧 👍 ❓
.. ✍🏼 ✍🏻✍🏿
.. ♀️✊ ⚖️ 📣
.. ✊🏻✊🏼✊🏽✊🏾✊🏿
.. 🤥 😍 ❤️
.. 🤪
.. ⚖️ 👨‍🎓
.. 🔥
.. 💧
.. ☠️
.. ☣️
.. 🪧
.. 🇺🇳
.. 🇫🇷
.. 🇧🇪
.. 🇺🇦
.. 🇬🇧
.. 🇨🇭
.. 🇩🇪 🎯
.. 🌍 ♀️✊🏽
.. 🇮🇷
.. 🎥 🎦
.. 🎇 🎉
.. un·e


|FluxWeb| `RSS <https://barathym.frama.io/barathym-2024/rss.xml>`_

.. figure:: images/entete.webp
   :width: 400

.. _barathym_2024:

===================================================================================
|barathym| **Barathym 2024**
===================================================================================

#Lebarathymcafé #barathymcafe #cci6 #passansnous #lequartier #villeneuve

.. toctree::
   :maxdepth: 6


   05/05

