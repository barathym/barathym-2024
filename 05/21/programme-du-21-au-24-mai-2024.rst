.. index::
   pair: Programme ; du 21 au 24 mai 2024

.. _programme_2024_05_21:

===================================================================================
2024-05-21 **Programme Barathym du 21 au 24 mai 2024**
===================================================================================

- :ref:`programme_2024_05_27`


.. figure:: images/programme.webp


Ça y est, ça tourne déjà au Barathym café ! Retrouvez le programme du 21 au 24 Mai
et participez à la nouvelle dynamique.

#barathymcafe #cci6 #passansnous #lequartier #villeneuve
