

.. _barathym_2024_05_06:

===================================================================================
2024-05-06 **Le Barathym café reouvre ses portes !**
===================================================================================


Le Barathym café reouvre ses portes !

Une assemblée générale extraordinaire a eu lieu le 03 Mai 2024 au
Patio 97 galerie de l’Arlequin avec la collaboration d’une dizaine d’associations
et avec le soutien du Conseil Citoyen Indépendants du secteur 6


.. figure:: images/ag.webp

.. figure:: images/femmes.webp
